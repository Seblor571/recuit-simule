﻿#pragma once
#include "Arete.h"
#include "Indexer.h"
#include "Graph.h"

template<class AV, class SV>
Graph<AV, SV> recuitSimule(Graph<int, string> &g) {

	g.init();

	Graph<AV, SV>* currentGraph(&g);
	int i;

	for (i = 0; i < 10; i++) {
		g.changementAleatoire();
		if (g.calculeCout() < currentGraph->calculeCout())
			*currentGraph = Graph<AV, SV>(g);
	}

	return *currentGraph;
}

int main() {
	srand(time(0));

	Indexer * indexer = Indexer::getInstance();

	Graph<int, string> *g = new Graph<int, string>();

	// Sommet(valeur, clé)
	Sommet<string> s1("Sommet 1", indexer->getCurrentIndex());
	cout << "s1: " << s1 << endl;
	Sommet<string> s2("Sommet 2", indexer->getCurrentIndex());
	cout << "s2: " << s2 << endl;
	Sommet<string> s3("Sommet 3", indexer->getCurrentIndex());
	cout << "s3: " << s3 << endl << endl;
	Sommet<string> s4("Sommet 4", indexer->getCurrentIndex());
	cout << "s4: " << s4 << endl << endl;
	Sommet<string> s5("Sommet 5", indexer->getCurrentIndex());
	cout << "s5: " << s5 << endl << endl;

	// Arête(début, fin, valeur, clé)
	Arete<int, string> a1(&s1, &s2, 6, indexer->getCurrentIndex());
	Arete<int, string> a2(&s2, &s3, 10, indexer->getCurrentIndex());
	Arete<int, string> a3(&s1, &s3, 5, indexer->getCurrentIndex());
	Arete<int, string> a4(&s1, &s4, 8, indexer->getCurrentIndex());
	Arete<int, string> a5(&s2, &s4, 1, indexer->getCurrentIndex());
	Arete<int, string> a6(&s3, &s4, 3, indexer->getCurrentIndex());
	Arete<int, string> a7(&s1, &s5, 15, indexer->getCurrentIndex());
	Arete<int, string> a8(&s2, &s5, 6, indexer->getCurrentIndex());
	Arete<int, string> a9(&s3, &s5, 4, indexer->getCurrentIndex());
	Arete<int, string> a10(&s4, &s5, 5, indexer->getCurrentIndex());


	g->addArete(&a1);
	g->addArete(&a2);
	g->addArete(&a3);
	g->addArete(&a4);
	g->addArete(&a5);
	g->addArete(&a6);
	g->addArete(&a7);
	g->addArete(&a8);
	g->addArete(&a9);
	g->addArete(&a10);

	cout << "Graph : " << g << endl << endl;

	Graph<int, string> * processingGraph;

	g->init();

	int coutActuel = g->calculeCout();
	int i = 0;

	for (i = 0; i < 5; i++) {
		processingGraph = new Graph<int, string>(*g);
		processingGraph->changementAleatoire();
		if (processingGraph->calculeCout() <= coutActuel) {
			g = new Graph<int, string>(*processingGraph);
			coutActuel = processingGraph->calculeCout();
		}
		cout << g->calculeCout() << endl;
	}

	cout << g->calculeCout() << endl;

	system("pause");
}