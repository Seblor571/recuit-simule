﻿class Indexer {
private:
	int _currentIndex;
	static Indexer *_instance;

	Indexer() : _currentIndex(0) {}
	~Indexer() {}


public:
	int getCurrentIndex() {
		return _currentIndex++;
	}

	static Indexer *getInstance() {
		if (!_instance) {
			_instance = new Indexer;
		}

		return _instance;
	}

};

Indexer *Indexer::_instance = NULL;