﻿#pragma once
#include <stdlib.h>
#include <iostream> 
#include <vector>
#include <ctime>
#include "Arete.h"

using namespace std;


template<class AV, class SV>
class Graph {

	vector<Arete<AV, SV>> _listAretes;
	vector<Arete<AV, SV>*> _chemin;
	vector<Sommet<SV>*> _listeSommets;
	vector<Sommet<SV>*> _sommetsVisites;

public:

	Graph() {
		_listAretes = vector<Arete<AV, SV>>();
		_chemin = vector<Arete<AV, SV>*>();
		_listeSommets = vector<Sommet<SV>*>();
		_sommetsVisites = vector<Sommet<SV>*>();
	}

	Graph(Graph<AV, SV> &g) {
		_listAretes = g._listAretes;
		_chemin = g._chemin;
		_listeSommets = g._listeSommets;
		_sommetsVisites = g._sommetsVisites;
	}

	void addArete(Arete<AV, SV> *a) {
		if (contains(a))
			return;

		_listAretes.push_back(*a);
		addSommet(a->_debut);
		addSommet(a->_fin);
	}
	void addSommet(Sommet<SV> *s) {
		if (!contains(s))
			_listeSommets.push_back(s);
	}

	void addAreteToPath(Arete<AV, SV> *a) {
		if (!pathContains(a))
			_chemin.push_back(a);
	}

	virtual ~Graph() {}

	bool contains(Arete<AV, SV> *a) {
		int i;
		for (i = 0; i < _listAretes.size(); i++) {
			if (*a == (_listAretes[i]))
				return true;
		}
		return false;
	};
	bool pathContains(Arete<AV, SV> *a) {
		int pos = find(_chemin.begin(), _chemin.end(), a) - _chemin.begin();
		return (pos != _chemin.size());
	};

	bool contains(Sommet<SV> *s) {
		int pos = find(_listeSommets.begin(), _listeSommets.end(), s) - _listeSommets.begin();
		return (pos != _listeSommets.size());
	};
	bool visited(Sommet<SV> *s) {
		int pos = find(_sommetsVisites.begin(), _sommetsVisites.end(), s) - _sommetsVisites.begin();
		return (pos != _sommetsVisites.size());
	};

	operator string() const {
		ostringstream oss;
		oss << "[";
		if (_listAretes.size() > 0) {
			oss << _listAretes[0];
			for (int i = 1; i < _listAretes.size(); i++) {
				oss << ", " << (string)_listAretes[i];
			}
		}
		oss << "]";
		return oss.str();
	}

	Arete<AV, SV> getElementAt(int index) {
		return _listAretes[index];
	}

	Arete<AV, SV> operator [] (int index) {
		return getElementAt(index);
	}

	Arete<AV, SV>* getNextInverted(Arete<AV, SV> * a) {
		if (a == NULL) {
			return NULL;
		}
		int i;
		for (i = 0; i < _chemin.size(); i++) {
			if ((a->_fin == _chemin[i]->_fin) && (a != _chemin[i]))
				return _chemin[i];
			if ((a->_fin == _chemin[i]->_debut) && (a != _chemin[i])) {
				_chemin[i]->invert();
				return _chemin[i];
			}
		}
		return NULL;
	}

	void init() {
		Sommet<SV> *firstS = _listeSommets[0];
		Sommet<SV> *currentS = _listeSommets[0];
		int i;
		Arete<AV, SV> a = _listAretes[0];
		do {

			// On cherche un arc ayant pour début ou fin le sommet courant.
			for (i = 0; i < _listAretes.size(); i++) {
				a = _listAretes[i];
				if (a._fin == currentS)
					a.invert();
				if (a._debut == currentS && !visited(a._fin)) {
					addAreteToPath(&_listAretes[i]);
					_sommetsVisites.push_back(a._debut);
					currentS = (a._fin);
					break;
				}
				else if (a._debut == currentS && a._fin == firstS && _sommetsVisites.size() == _listeSommets.size() - 1) {
					addAreteToPath(&_listAretes[i]);
					currentS = (a._fin);
					break;
				}
			}
		} while (*firstS != *currentS);

		_chemin.back()->invert();

		for (Arete<AV, SV>* a : _chemin) {
			cout << "chemin :" << *a << endl;
		}
		cout << endl;
	}

	void changementAleatoire() {
		int index1 = rand() % _chemin.size(), index2;
		do {
			index2 = rand() % _chemin.size();
		} while ((abs(index1 - index2) <= 1)
			|| (index1 == 0 && index2 == _chemin.size() - 1)
			|| (index2 == 0 && index1 == _chemin.size() - 1));

		Sommet<SV> *A = _chemin[index1]->_debut;
		Sommet<SV> *B = _chemin[index2]->_debut;
		Sommet<SV> *C = _chemin[index1]->_fin;
		Sommet<SV> *D = _chemin[index2]->_fin;

		_chemin.erase(_chemin.begin() + index1); // effacer AC
		_chemin.push_back(findArete(A, B)); // remplacer par AB

		int secureVar = 0; // éviter les boucles infinies

		Arete<AV, SV>* processingArete = findAreteInPathEndingWith(B);
		do {
			processingArete->invert();
			processingArete = getNextInverted(processingArete);
			secureVar++;
		} while ((secureVar < _chemin.size()) && (processingArete != NULL) && *(processingArete->_debut) != *C);
		processingArete->invert();

		_chemin.erase(_chemin.begin() + index2 - (index1 < index2)); // effacer BD
		_chemin.push_back(findArete(C, D)); // remplacer par CD

	}

	Arete<AV, SV>* findArete(Sommet<SV>* s1, Sommet<SV>* s2) {
		int i;
		for (i = 0; i < _listAretes.size(); i++) {
			if (_listAretes[i]._debut == s2 && _listAretes[i]._fin == s1)
				_listAretes[i].invert();
			if ((_listAretes[i]._debut == s1 && _listAretes[i]._fin == s2))
				return &(_listAretes[i]);
		}
		return NULL;
	}

	Arete<AV, SV>* findAreteInPathEndingWith(Sommet<SV>* s) {
		int i;
		for (i = 0; i < _chemin.size(); i++) {
			if (_chemin[i]->_fin == s)
				return (_chemin[i]);
		}
		return NULL;
	}

	AV calculeCout() {
		AV somme = 0;
		for (Arete<AV, SV> *a : _chemin) {
			somme += a->_v;
		}
		return somme;
	}

	Graph<AV, SV> &Graph<AV, SV>::operator = (const Sommet<SV> &g) {
		return new Graph<AV, SV>(&g);
	}

};

template<class AV, class SV>
inline ostream & operator<< (ostream & oss, const Graph<AV, SV> &g) {
	return oss << (string)g;
}