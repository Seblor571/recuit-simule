#pragma once
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class GElement {
public:
	int _clef;

	GElement(const int clef) : _clef(clef) {
	}

	virtual ~GElement() {}

	operator string() const {
		return to_string(_clef);
	}

};

inline ostream & operator<< (ostream & oss, const GElement &ge) {
	return oss << (string)ge;
}