﻿#pragma once
#include "GElement.h"

template<class SV>
class Sommet : public GElement {
public:
	int _degre;
	SV _v;

	Sommet(const SV &v, const int clef) : GElement(clef), _degre(NULL), _v(v) {}

	virtual ~Sommet() {}

	operator string() const {

		ostringstream oss;

		oss << "[clef: " << this->_clef << ", degre: " << _degre << ", v: " << _v << "]";

		return oss.str();
	}

	bool operator == (const Sommet<SV> &s) {
		return this->_clef == s._clef;
	}

	bool operator != (const Sommet<SV> &s) {
		return this->_clef != s._clef;
	}

};

template<class SV>
inline ostream & operator<< (ostream & oss, const Sommet<SV> &s) {
	return oss << (string)s;
}