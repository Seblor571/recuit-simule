﻿#pragma once
#include "Sommet.h"

template<class AV, class SV>
class Arete : public GElement {
public:
	Sommet<SV> *_debut;
	Sommet<SV> *_fin;
	AV _v;

	Arete(Sommet<SV> *debut, Sommet<SV> *fin, const AV &v, const int clef) : GElement(clef), _debut(debut), _fin(fin), _v(v) {}

	virtual ~Arete() {}

	bool estEgal(const Sommet<SV> *s1, const Sommet<SV> *s2) const {
		return ((s1 == _debut && s2 == _fin) ||
			(s1 == _fin && s2 == _debut));
	}

	bool contains(const Sommet<SV> *s) {
		return (s == _debut || s == _fin);
	}

	void invert() {
		if (this == NULL) {
			return;
		}
		Sommet<SV> *temp = this->_debut;
		this->_debut = this->_fin;
		this->_fin = temp;
	}

	operator string() const {

		ostringstream oss;

		oss << "[GElement: " << (GElement)*this << ", poids: " << _v << ", debut: " << _debut->_clef << ", fin: " << _fin->_clef << "]";

		return oss.str();
	}

	bool operator == (const Arete<AV, SV> &a) {
		return this->_clef == a._clef;
	}

};

template<class AV, class SV>
inline ostream & operator<< (ostream & oss, const Arete<AV,SV> &a) {
	return oss << (string)a;
}